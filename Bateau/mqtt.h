#ifndef MQTT_H
#define MQTT_H

#include <QtMqtt/QMqttClient>


class MQTT
{
private:
    QMqttClient *m_client;


public:
    QMqttClient *m_serveur;
    MQTT();
    ~MQTT();
    bool connectionHote(QString hostname);
    bool subscribe();
    bool publish(QByteArray message);
};

#endif // MQTT_H
