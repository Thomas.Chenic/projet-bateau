#ifndef BATEAU_H
#define BATEAU_H
#include<iostream>
#include "mqtt.h"
#include <QMainWindow>
#include "battement.h"

QT_BEGIN_NAMESPACE
namespace Ui { class bateau; }
QT_END_NAMESPACE

class bateau : public QMainWindow
{
    Q_OBJECT

    QByteArray text;
    Battement *m_bat;
public:
    bateau(QWidget *parent = nullptr);
    ~bateau();

    // Constructeur pour initialiser les coordonnées
        bateau(int initialLatitude, int initialLongitude) : latitude(initialLatitude), longitude(initialLongitude) {}

        void modificationCap(const QString &message);

private slots:

    void on_CAP_sliderMoved(int position);

    void on_Vitesse_sliderMoved(int position);

    void on_DirectionVent_sliderMoved(int position);

    void on_VitesseVent_sliderMoved(int position);

    void on_AngleBarre_sliderMoved(int position);

    void on_pushButtonPosition_pressed();



    void on_CAP_2_sliderMoved(int position);

    void on_Vitesse_2_sliderMoved(int position);

    int inverserDegres(int valeur);

    void on_AngleBarre_2_sliderMoved(int position);


private:
    Ui::bateau *ui;
    MQTT *m_mqtt;
    double latitude = 46.490225; // Coordonnées du bateau 1
    double longitude = -1.78;

    double latitude2 = 46.490225; // Coordonnées du bateau 2
    double longitude2 = -1.783;
};
#endif // BATEAU_H
